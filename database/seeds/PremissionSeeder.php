<?php

use Illuminate\Database\Seeder;
use Corp\Permission;

class PremissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ['name' => 'VIEW_ADMIN'],
            ['name' => 'ADD_ARTICLES'],
            ['name' => 'UPDATE_ARTICLES'],
            ['name' => 'DELETE_ARTICLES'],
            ['name' => 'ADMIN_USERS'],
            ['name' => 'VIEW_ADMIN_ARTICLES'],
            ['name' => 'EDIT_USERS'],
        ];

        foreach($permissions as $item)
        {
            $permission = new Permission();
            $permission->name = $item['name'];
            $permission->save();
        }
    }
}
