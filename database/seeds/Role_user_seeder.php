<?php

use Illuminate\Database\Seeder;
use Corp\Role_user;

class Role_user_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_users = [
            ['user_id' => 1, 'role_id' => 1],
        ];

        foreach ($role_users as $item)
        {
            $role_user = new Role_user();
            $role_user->user_id = $item['user_id'];
            $role_user->role_id = $item['role_id'];
            $role_user->save();
        }
    }
}
