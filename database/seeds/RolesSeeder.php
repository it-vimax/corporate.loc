<?php

use Illuminate\Database\Seeder;
use Corp\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name' => 'Admin'],
            ['name' => 'Moderator'],
            ['name' => 'Guest'],
        ];

        foreach($roles as $item)
        {
            $role = new Role();
            $role->name = $item['name'];
            $role->save();
        }
    }
}
