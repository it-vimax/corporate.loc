<?php

use Illuminate\Database\Seeder;
use Corp\Permission_role;

class Premission_roleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_roles = [
            ['role_id' => '1', 'permission_id' => '1'],
            ['role_id' => '1', 'permission_id' => '2'],
            ['role_id' => '1', 'permission_id' => '3'],
            ['role_id' => '1', 'permission_id' => '4'],
            ['role_id' => '1', 'permission_id' => '5'],
            ['role_id' => '1', 'permission_id' => '6'],
            ['role_id' => '1', 'permission_id' => '7'],
        ];

        foreach($permission_roles as $item)
        {
            $permission_role = new Permission_role();
            $permission_role->role_id = $item['role_id'];
            $permission_role->permission_id = $item['permission_id'];
            $permission_role->save();
        }
    }
}
